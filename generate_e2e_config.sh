#!/usr/bin/env sh

environmentSlug=$1
triggerBranch=$2

# Generate the child pipeline!!
echo """
run_e2e_test_${environmentSlug}_${triggerBranch}:
  variables:
    PYTEST_TEST_FILTER: 'not RT'
    ENV: ${environmentSlug}
  trigger:
    project: aon/ips/quality-assurance/api-packages/entity-service
    branch: '${triggerBranch}'
    strategy: depend
""" > generated_e2e_pipeline_${environmentSlug}.yml
